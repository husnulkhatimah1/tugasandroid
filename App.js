import React, {Component} from "react";
import { SafeAreaView, StyleSheet, TextInput, Button, Text } from "react-native";


class App extends Component {
  state = {
    nama:"",
    no_hp:""
  }
  namaTextChange = (inputText) =>{
    this.setState({nama : inputText})
  }

  no_hpTextChange = (inputText) =>{
    this.setState({no_hp : inputText})
  }

  hasil = () => {
    alert (this.state.nama +" "+":"+" "+ this.state.no_hp)
  }
  render() {
    return (
      <SafeAreaView>
        <TextInput
          style={styles.input}
          onChangeText={this.namaTextChange}
          placeholder = "masukkan nama"
        />
        <TextInput
          style={styles.input}
          onChangeText={this.no_hpTextChange}
          placeholder = "masukkan no hp"
        />
        <Button
          title="tekan saya"
          onPress={this.hasil}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
});

export default App;